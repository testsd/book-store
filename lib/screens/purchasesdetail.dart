import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../components/cartcard.dart';
import '../components/purcard.dart';
import '../data/receipt.dart';
import '../models/fooderlich_pages.dart';
import '../models/purchase_manager.dart';
import 'comfirm_order.dart';

class PurDetail extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FooderlichPages.purchase,
      key: ValueKey(FooderlichPages.purchase),
      child: const PurDetail(),
    );
  }

  const PurDetail({
    Key? key,
  }) : super(key: key);

  @override
  State<PurDetail> createState() => _PurDetailState();
}

class _PurDetailState extends State<PurDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Provider.of<PurPageManager>(context, listen: false)
                .tapOnCheckout(false);
            Navigator.popUntil(context, ModalRoute.withName('/purchase'));
          },
          icon: Icon(Icons.close),
        ),
        backgroundColor: Colors.white,
        title: const Text(
          'Purchase Detail',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 15,
          ),
          const SizedBox(
            height: 80,
            child: Center(
              child: Icon(
                Icons.receipt,
                size: 60,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(40, 10, 10, 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '----------------------------'
                      '---------------------------------------------------'
                      '---'
                ),
                Text(
                    ''
                        ''
                ),
                Text(
                  'Purchase ID: ' + forDetail.purID,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 2,
                ),
                Text(
                  'Name: ' + forDetail.name,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 2,
                ),
                Text(
                  'Address: ' + forDetail.address,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 2,
                ),
                Text(
                  'Phone number: ' + forDetail.tel,
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 2,
                ),
                Text(
                  'Total price: ' + forDetail.totalPrice.toString() + ' THB',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(
                  height: 2,
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              padding: const EdgeInsets.all(0),
              itemCount: forDetail.order.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 70,
                  width: 70,
                  color: Colors.white,
                  child: CardItem2(
                    order: forDetail.order[index],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
