import 'package:flutter/material.dart';
import 'package:fooderlich/screens/view_items.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:provider/provider.dart';

import 'bookdetail.dart';
import '../components/cartcard.dart';
import '../data/order.dart';
import '../data/receipt.dart';
import '../models/cart_manager.dart';
import '../models/fooderlich_pages.dart';
import '../models/profile_manager.dart';

class ConfirmOrder extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FooderlichPages.order,
      key: ValueKey(FooderlichPages.order),
      child: const ConfirmOrder(),
    );
  }

  const ConfirmOrder({Key? key}) : super(key: key);

  @override
  State<ConfirmOrder> createState() => _ConfirmOrderState();
}

class _ConfirmOrderState extends State<ConfirmOrder> {
  final name = TextEditingController();
  final address = TextEditingController();
  final tel = TextEditingController();
  SingingCharacter? _character = SingingCharacter.lafayette;

  @override
  void dispose() {
    name.dispose();
    address.dispose();
    tel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: () {
          Provider.of<CartPageManager>(context, listen: false)
              .tapOnCheckout(false);
          isChecked = false;
          Navigator.popUntil(context, ModalRoute.withName('/cart'));
          selectedOrder = [];
        }, icon: Icon(Icons.arrow_back_ios_outlined) ,

        ),
        backgroundColor: Colors.white,
        title: const Text(
          'Order Detail',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(
            height: 10,
          ),
          const Text(
            '  Enter your information',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            textAlign: TextAlign.left,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 16),
            child: TextFormField(
              controller: name,
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Name',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 16),
            child: TextFormField(
              controller: address,
              decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Address',
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 14, vertical: 16),
            child: IntlPhoneField(
              controller: tel,
              decoration: const InputDecoration(
                labelText: 'Phone Number',
                border: UnderlineInputBorder(
                  borderSide: BorderSide(),
                ),
              ),
              initialCountryCode: 'TH',
              onChanged: (phone) {
                print(phone.completeNumber);
              },
            ),
          ),
          Column(
            children: <Widget>[
              ListTile(
                title: const Text('Cash on delivery'),
                leading: Radio<SingingCharacter>(
                  value: SingingCharacter.lafayette,
                  groupValue: _character,
                  onChanged: (SingingCharacter? value) {
                    setState(() {
                      _character = value;
                    });
                  },
                ),
              ),
              ListTile(
                title: const Text('Debit/Credit card'),
                leading: Radio<SingingCharacter>(
                  value: SingingCharacter.jefferson,
                  groupValue: _character,
                  onChanged: (SingingCharacter? value) {
                    setState(() {
                      _character = value;
                    });
                  },
                ),
              ),
            ],
          ),
          Row(
            children: [
              const SizedBox(
                width: 30,
              ),
              const Icon(Icons.notes),
              const Padding(
                padding: EdgeInsets.fromLTRB(8, 2, 0, 2),
                child: Text(
                  ' Item list',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 19,
                    color: Colors.black,
                  ),
                ),
              ),
              const SizedBox(
                width: 150,
              ),
              TextButton(
                onPressed: () => {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const ViewItem(),
                    ),
                  )
                },
                child: Text(
                  ' view all' '(${selectedOrder.length})',
                  textAlign: TextAlign.right,
                  style: const TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 15,
                    color: Colors.blueGrey,
                  ),
                ),
              )
            ],
          ),
          ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: const EdgeInsets.all(8),
            itemCount: 1,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                width: 50,
                color: Colors.white,
                child: CardItem2(
                  order: selectedOrder[0],
                ),
              );
            },
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              ' Total price: ' + total(selectedOrder).toString() + ' THB ',
              textAlign: TextAlign.right,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.brown,
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: ButtonTheme(
        buttonColor: Colors.black,
        minWidth: 350.0,
        height: 40.0,
        child: RaisedButton(
          child: const Text(
            'Place Order',
            style: TextStyle(fontSize: 16, color: Colors.white),
          ),
          onPressed: () {
            String purID = randomID();
            var c = Receipt(purID, name.text, address.text, tel.text,
                selectedOrder.length, total(selectedOrder), selectedOrder);
            receipt.add(c);
            print('Total Receipt ' + c.totalPrice.toString());
            for (int f = 0; f < selectedOrder.length; f++) {
              for (int i = 0; i < listOrder.length; i++) {
                // ignore: lines_longer_than_80_chars
                print('Book: ' +
                    selectedOrder[f].getName +
                    ' | ' +
                    listOrder[i].getName);
                if (listOrder[i].getName == selectedOrder[f].getName) {
                  // ignore: lines_longer_than_80_chars
                  print('Deleted: ' +
                      listOrder[i].getName +
                      'amount: ' +
                      listOrder[i].getAmount.toString());
                  listOrder.removeAt(i);
                }
              }
            }

            Provider.of<CartPageManager>(context, listen: false)
                .tapOnCheckout(false);
            isChecked = false;
            Navigator.popUntil(context, ModalRoute.withName('/cart'));
            selectedOrder = [];
          },
        ),
      ),
    );
  }
}

enum SingingCharacter { lafayette, jefferson }

class CardItem2 extends StatefulWidget {
  final Order order;

  const CardItem2({Key? key, required this.order}) : super(key: key);

  @override
  State<CardItem2> createState() => _CardItem2();
}

class _CardItem2 extends State<CardItem2> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Row(
        children: [
          Expanded(
            flex: 4,
            child: Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
              ),
              width: 30,
              child: Image.asset(widget.order.getImg),
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              widget.order.getName,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
              textAlign: TextAlign.start,
            ),
          ),
          Text(
            '  \฿${widget.order.getTotalPrice} ',
            style: const TextStyle(
                color: Colors.green, fontWeight: FontWeight.w600),
            textAlign: TextAlign.left,
          ),
          Text(
            '[ \฿${widget.order.getPrice}X${widget.order.getAmount} ]',
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.w600),
            textAlign: TextAlign.left,
          ),
        ],
      ),
    );
  }
}
