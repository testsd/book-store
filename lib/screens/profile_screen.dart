import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'bookdetail.dart';
import '../components/booklist.dart';
import '../models/books.dart';
import '../models/models.dart';

class BookScreen extends StatefulWidget {
  static MaterialPage page() {
    return MaterialPage(
      name: FooderlichPages.bookdetail,
      key: ValueKey(FooderlichPages.bookdetail),
      child: BookScreen(),
    );
  }

  const BookScreen({
    Key? key,
  }) : super(key: key);

  @override
  _BookScreenState createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.close),
          onPressed: () {
            Provider.of<BookPageManager>(context, listen: false)
                .tapOnProfile(false);
            selectBook=[];
          },
        ),
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Detail(book: selectBook.last,)
          ],
        ),
      ),
    );
  }
}
