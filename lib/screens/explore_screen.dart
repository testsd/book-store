import 'package:flutter/material.dart';

import '../api/mock_fooderlich_service.dart';
import '../components/booklist.dart';
import 'search.dart';
import '../models/books.dart';
import '../models/explore_data.dart';
import '../models/models.dart';

class ExploreScreen extends StatelessWidget {
  final mockService = MockFooderlichService();

  ExploreScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: mockService.getExploreData(),
      builder: (context, AsyncSnapshot<ExploreData> snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return ListView(
            scrollDirection: Axis.vertical,
            children: [
              Mathematics(books: snapshot.data?.math ?? [], ),
              const SizedBox(height: 16),
              English(books: snapshot.data?.english ?? [], ),
              const SizedBox(height: 16),
              Computer(books: snapshot.data?.computer ?? [], ),
            ],
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

