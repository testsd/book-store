
import 'package:flutter/material.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

import '../components/cartcard.dart';

import 'comfirm_order.dart';

class ViewItem extends StatefulWidget {
  const ViewItem({Key? key}) : super(key: key);

  @override
  State<ViewItem> createState() => _ViewItemState();
}

class _ViewItemState extends State<ViewItem> {
  final name = TextEditingController();
  final address = TextEditingController();
  final tel = TextEditingController();
  SingingCharacter? _character = SingingCharacter.lafayette;

  @override
  void dispose() {
    name.dispose();
    address.dispose();
    tel.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          'Items',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView(
        children: [
          ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            padding: const EdgeInsets.all(8),
            itemCount: selectedOrder.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                height: 50,
                width: 50,
                color: Colors.white,
                child: CardItem2(
                  order: selectedOrder[index],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

