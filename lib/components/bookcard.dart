
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../models/books.dart';


class BookCard extends StatelessWidget {
  final Book book;
  final Function() press;

  const BookCard({Key? key, required this.book, required this.press}) :
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                color: Colors.white60,
                borderRadius: BorderRadius.circular(16),
              ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(15.0),
                  child:Image.asset(book.img,),),


            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(4, 5, 10, 5),
            child: Text(
              book.name,
              style: const TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          Text(
            '  \฿${book.price}',
            style: const TextStyle(
                color: Colors.green, fontWeight: FontWeight.w600),
          ),
        ],
      ),
    );
  }
}
