
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../data/order.dart';
import '../data/receipt.dart';
import '../models/purchase_manager.dart';
import '../screens/purchasesdetail.dart';
import '../screens/bookdetail.dart';

class PurCard extends StatefulWidget {
  final Receipt receipt;
  final Function() press;

  const PurCard({Key? key, required this.receipt, required this.press})
      : super(key: key);

  @override
  State<PurCard> createState() => _PurCardState();
}
dynamic forDetail = null;
class _PurCardState extends State<PurCard> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.press,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: IconButton(
              icon: Icon(Icons.assignment_turned_in),
              onPressed: () {
                forDetail = widget.receipt;
                Provider.of<PurPageManager>(context, listen: false)
                    .tapOnCheckout(true);
                },
              iconSize: 45,
            ),
          ),
          Expanded(
            flex: 6,
            child: Column(
              children: [
                Align(
                  child: Text(
                    'ID: ' + widget.receipt.purID,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  alignment: Alignment.centerLeft,
                ),
                Align(
                  child: Text(
                    'Order date: ' + date,
                    style: TextStyle(fontWeight: FontWeight.normal),
                  ),
                  alignment: Alignment.centerLeft,
                ),
              ],
            ),
          ),
          Expanded(
            flex: 2,
            child: Row(
              children: const [
                Icon(
                  Icons.arrow_back_ios,
                  size: 30,
                  color: Colors.grey,
                ),
                Icon(
                  Icons.delete_forever_outlined,
                  size: 30,
                  color: Colors.grey,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
