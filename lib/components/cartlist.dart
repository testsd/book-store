
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/app_state_manager.dart';
import '../screens/cart_screen.dart';
import '../screens/bookdetail.dart';
import 'cartcard.dart';

class CartListEmpty extends StatelessWidget {
  const CartListEmpty({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: Card(
          child: SizedBox(
            width: 300,
            height: 300,
            child: Column(
              children: const [
                Icon(Icons.shopping_basket_outlined, size: 190,),
                Text(
                  'You have no items in your shopping cart',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                ),

              ],
            ),
          ),
        ),
      ),
    );
  }
}

class CartListBody extends StatelessWidget {
  const CartListBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: listOrder.length,
        itemBuilder: (BuildContext context, int index) {
          return Dismissible(
            background: Container(
                color: Colors.red,
                child: const Align(
                  alignment: Alignment.center,
                  child: Text(
                    'Deleted',
                    textAlign: TextAlign.right,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                )),
            confirmDismiss: (direction) async {
              if (direction == DismissDirection.endToStart || direction ==
                  DismissDirection.startToEnd) {
                Navigator.pop(context);
                print(index);
                print(listOrder.length);
                listOrder.removeAt(index);
                Provider.of<AppStateManager>(context, listen: false)
                    .goToTab(index);
                return true;
              }
            },
            key: UniqueKey(),
            child: CardOfListPage(order: listOrder[index], press: () {}),
          );
        },
      ),
    );
  }
}
