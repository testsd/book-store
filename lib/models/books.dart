
class Book {
  final String id;
  final String name;
  final String img;
  final String writer;
  final double price;
  final String des;

  const Book({
    required this.id,
    required this.name,
    required this.img,
    required this.writer,
    required this.price,
    required this.des,
  });
  factory Book.fromJson(Map<String, dynamic> json) {
    return Book(
      id: json['id'] ?? '',
      name: json['name'] ?? '',
      img: json['img'] ?? '',
      writer: json['writer'] ?? '',
      price: json['price'] ?? '',
      des: json['des'] ?? '',
    );
  }
}


