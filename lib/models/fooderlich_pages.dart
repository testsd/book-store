class FooderlichPages {
  static String splashPath = '/splash';
  static String loginPath = '/login';
  static String onboardingPath = '/onboarding';
  static String home = '/';
  static String bookdetail = '/item';
  static String cart = '/cart';
  static String order = '/order';
  static String purchase = '/purchase';
  static String booklist='/booklist';
}
