import 'books.dart';
import 'models.dart';

class ExploreData {
  final List<Book> math;
  final List<Book> english;
  final List<Book> computer;
  final List<Map<String, dynamic>> allBook = [
    {
      'id': '1',
      'name': 'How Computers Work',
      'img': 'assets/bestseller/com01.jpg',
      'writer': 'Matthew Justice',
      'price': 200.00,
      'des': 'An approachable, hands-on guide to understanding how computers '
          'work, from low-level circuits to high-level code.'

    },
    {
      'id': '2',
      'name': '100 Things ',
      'img': 'assets/bestseller/com02.jpg',
      'writer': 'Matthew Justice',
      'price': 240.00,
      'des': 'Various Shortlisted - Royal Society Young People\'s Book Prize '
          '2019'

    },
    {
      'id': '3',
      'name': 'My Revision Notes',
      'img': 'assets/bestseller/com03.jpg',
      'writer': 'Mark Clarkson',
      'price': 190.00,
      'des': 'Set your students on track to achieve the best grade possible '
          'with '
          'My Revision Notes: AQA A-level Computer Science.'

    },
    {
      'id': '4',
      'name': 'Algorithms Java',
      'img': 'assets/bestseller/com04.jpg',
      'writer': 'yang hu',
      'price': 240.00,
      'des': 'This book is rich in examples, with beautiful pictures and '
          'texts, '
          'and explains the data structure and algorithms in a way that is '
          'easy '
          'to understand.'

    },
    {
      'id': '5',
      'name': 'Coding For Kids Scratch',
      'img': 'assets/bestseller/com05.jpg',
      'writer': 'Tommy Wilson',
      'price': 140.00,
      'des': 'Learn to code awesome games with Scratch. Stay ahead of the '
          'curve '
          'with this fully updated guide for beginner coders.'

    },
    {
      'id': '6',
      'name': 'Scratch Coding Cards',
      'img': 'assets/bestseller/com06.jpg',
      'writer': 'Natalie Rusk',
      'price': 320.00,
      'des': 'You can carry this book where ever you want. It is easy to '
          'carry. '
          'It can be an ideal gift to yourself and to your loved ones.'

    },
    {
      'id': '7',
      'name': 'Watching the English',
      'img': 'assets/bestseller/eng01.jpg',
      'writer': 'Kate Fox',
      'price': 200.00,
      'des': 'Kate Fox takes a revealing look at the quirks, habits and '
          'foibles '
          'of the English people.'

    },
    {
      'id': '8',
      'name': 'English Grammar',
      'img': 'assets/bestseller/eng02.jpg',
      'writer': 'Michael McCarthy',
      'price': 230.00,
      'des': 'The Basics offers a clear, non-jargonistic introduction to '
          'English '
          'grammar and its place in society.'

    },
    {
      'id': '9',
      'name': 'English Grammar',
      'img': 'assets/bestseller/eng03.jpg',
      'writer': 'Sean Williams',
      'price': 156.00,
      'des': 'The practical English grammar guide for perfect writing.'

    },
    {
      'id': '10',
      'name': 'The English Tenses',
      'img': 'assets/bestseller/eng04.jpg',
      'writer': 'Phil Williams',
      'price': 230.00,
      'des': 'Improve your grammar skills with 161 focused exercises.'

    },
    {
      'id': '11',
      'name': 'English Grammar Rules',
      'img': 'assets/bestseller/eng05.jpg',
      'writer': 'Farlex International',
      'price': 340.00,
      'des': 'The grammar book for the 21st century has arrived, from the '
          'language experts at Farlex International and TheFreeDictionary.com, '
          'the trusted reference destination with 1 billion+ annual visits.'

    },
    {
      'id': '12',
      'name': 'Daily Sentence Structures',
      'img': 'assets/bestseller/eng06.jpg',
      'writer': 'Alec Lees',
      'price': 320.00,
      'des': 'This practical guide provides a straightforward way of teaching '
          'pupils to use a range of sentence structures in their own writing, '
          'whatever their bility.'

    },
    {
      'id': '13',
      'name': 'Verb Types and Tenses',
      'img': 'assets/bestseller/eng07.jpg',
      'writer': 'Kevin Kirk',
      'price': 215.00,
      'des': 'If you have ever struggled with choosing exactly what tense or '
          'verb type you must use in your writing, then this is the book for '
          'you.'

    },
    {
      'id': '14',
      'name': 'English Verbs',
      'img': 'assets/bestseller/eng08.jpg',
      'writer': 'Mark Lester',
      'price': 120.00,
      'des': 'This comprehensive guide is your one-stop resource for learning '
          'English verbs. '

    },
    {
      'id': '15',
      'name': 'Maths Activity Book',
      'img': 'assets/bestseller/math01.jpg',
      'writer': 'kevin',
      'price': 100.00,
      'des': '100 pages of maths activities + fun challenges = a head start '
          'for '
          'your child'

    },
    {
      'id': '16',
      'name': 'Super Simple Maths',
      'img': 'assets/bestseller/math02.jpg',
      'writer': 'DK',
      'price': 130.00,
      'des': 'All the core national curriculum maths topics in one book, an '
          'accessible, and indispensable guide for students, parents, '
          'and educators.'

    },
    {
      'id': '17',
      'name': "What\'s the Point?",
      'img': 'assets/bestseller/math03.jpg',
      'writer': 'DK',
      'price': 190.00,
      'des': 'Maths makes the world go around. An educational book that will '
          'give you surprising answers to everyday maths challenges.'

    },
    {
      'id': '18',
      'name': 'Calculus',
      'img': 'assets/bestseller/math04.jpg',
      'writer': 'Morris Kline',
      'price': 240.00,
      'des': '- no description -'

    },
    {
      'id': '19',
      'name': 'Statistics',
      'img': 'assets/bestseller/math05.jpg',
      'writer': 'Alan Graham',
      'price': 210.00,
      'des': 'this book makes often complex concepts and techniques easy to '
          'get to grips with. This new edition has been fully updated.'

    },
    {
      'id': '20',
      'name': 'The Joy of X',
      'img': 'assets/bestseller/math06.jpg',
      'writer': 'Steven H. Strogatz',
      'price': 320.00,
      'des': "it has written a witty and fascinating account of maths\' most "
          'compelling ideas and how, so often, they are an integral part of '
          'everyday life.'

    }
  ];

  ExploreData(
      this.math,
      this.english ,
      this.computer ,
  );
}
