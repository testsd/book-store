import 'package:flutter/material.dart';

import 'books.dart';
import 'models.dart';

class PurPageManager extends ChangeNotifier {
  bool get didSelect => _didSelect;
  bool get didSelectCheckout => _checkout;

  var _didSelect = false;
  var _checkout = false;

  void tapOnComplete(bool x) {
    _didSelect = x;
    notifyListeners();
  }
  void tapOnCheckout(bool x) {
    _checkout = x;
    notifyListeners();
  }

}
