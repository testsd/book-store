import 'package:flutter/material.dart';

import 'books.dart';
import 'models.dart';

class BookPageManager extends ChangeNotifier {
  bool get didSelectBook => _didSelectBook;
  bool get didTapOnAddtoCart => _tapOnAddtoCart;
  bool get darkMode => _darkMode;

  var _didSelectBook = false;
  var _tapOnAddtoCart = false;
  var _darkMode = false;


  void tapOnRaywenderlich(bool selected) {
    _tapOnAddtoCart = selected;
    notifyListeners();
  }

  void tapOnProfile(bool selected) {
    _didSelectBook = selected;
    notifyListeners();
  }
}
